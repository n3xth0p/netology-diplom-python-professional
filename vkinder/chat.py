import vk_api
from random import randrange


def write_msg(vk: vk_api, user_id, message, attachment_list=None):
    if attachment_list is None or len(attachment_list) == 0:
        vk.method('messages.send', {'user_id': user_id, 'message': message, 'random_id': randrange(10 ** 7), })
    else:
        attach = [x['url'] for x in attachment_list]
        vk.method('messages.send', {'user_id': user_id, 'message': message,
                                    'random_id': randrange(10 ** 7),
                                    'attachment': ','.join(attach)
                                    })

import time
from vkinder.parser import parse_photos
from vkinder.input_data import UserInput
from vkinder.db import get_saved_people, save_top_people
from vkinder.settings import TIME_DELAY_REQUEST


def people_search(data: UserInput, count=100, status=6) -> list:
    result = get_people_vk_search(data, count=count, status=status)
    time.sleep(2 * TIME_DELAY_REQUEST)
    skip_list = get_saved_people()
    parsed_people = list()
    if result['count'] == 0:
        return []
    print("please wait")
    for item in result['items']:
        if not item['is_closed'] and item['id'] not in skip_list:
            temp_dict = dict()
            temp_dict['id'] = item['id']
            temp_dict['profile_url'] = f"https://vk.com/id{item['id']}"
            temp_dict['firstname'] = item['first_name']
            temp_dict['lastname'] = item['last_name']

            photos = get_profile_photo(data, item['id'])
            temp_dict['photo'] = parse_photos(photos)
            time.sleep(TIME_DELAY_REQUEST)
            print(".", end='')
            try:
                mutual_friends = get_mutual_friends(data, item['id'])
                temp_dict['mutual'] = len(mutual_friends)
            except Exception:
                temp_dict['mutual'] = 0

            time.sleep(TIME_DELAY_REQUEST)
            parsed_people.append(temp_dict)
        top_parsed_people = sorted(parsed_people, key=lambda x: x['mutual'], reverse=True)[0:3]
    save_top_people(top_parsed_people)
    return top_parsed_people


def get_people_vk_search(data: UserInput, count=100, status=6):
    result = data.vk_api.users.search(country_id=data.country_id, v=data.api_version,
                                      count=count, city=data.city_id, sex=data.sex, age_to=data.age_to,
                                      age_from=data.age_from, status=status, sort=0)
    return result


def get_profile_photo(data: UserInput, id: str):
    photos = data.vk_api.photos.get(v=data.api_version, count=100, album_id='profile', owner_id=id, extended=1)
    return photos


def get_mutual_friends(data: UserInput, id: str):
    mutual = data.vk_api.friends.getMutual(v=data.api_version, source_uid=data.user_profile_id, target_uid=id)
    return mutual

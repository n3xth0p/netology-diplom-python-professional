def parse_photos(data: list) -> list:
    result = list()
    if len(data) == 0:
        return []
    else:
        for item in data['items']:
            temp_dict = {'likes': item['likes']['count'],
                         'url': f"photo{item['owner_id']}_{item['id']}"}
            result.append(temp_dict)
        return sorted(result, key=lambda x: x['likes'], reverse=True)[0:3]

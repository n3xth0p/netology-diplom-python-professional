import vk
import time
from vk.exceptions import VkException, VkAuthError, VkAPIError
from vkinder.settings import TIME_DELAY_REQUEST, APPLICATION_TOKEN, VK_API_VERSION
from vkinder.chat import write_msg


class UserInput:

    def __init__(self, token=APPLICATION_TOKEN, api_version=VK_API_VERSION):
        self.api_version = api_version
        self.api_token = token
        self.api_session = vk.Session(self.api_token)
        self.vk_api = vk.API(self.api_session)
        self.group_vk_api_session = None
        self.user_profile_id = None
        self.message_user_id = None
        self.sex = None
        self.city_id = None
        self.city_title = None
        self.country_id = 1
        self.age_from = None
        self.age_to = None
        self.current_state = 'initial'

    def user_input_profile_id(self):
        write_msg(self.group_vk_api_session, self.message_user_id, f'Введите id профиля для которого ищем пару: ')

    def check_profile(self, user_profile_id: str) -> bool:
        try:
            r = self.vk_api.users.get(user_id=user_profile_id, v=self.api_version)
            time.sleep(TIME_DELAY_REQUEST)
            if 'deactivated' in r[0].keys():
                return False
            else:
                return True
        except (VkException, VkAuthError, VkAPIError):
            return False

    def user_input_city(self):
        write_msg(self.group_vk_api_session, self.message_user_id, f'Введите город: ')

    def get_vk_city_id_by_name(self, city_name: str) -> bool:
        try:
            r = self.vk_api.database.getCities(country_id=self.country_id, q=city_name, v=self.api_version)
            self.city_id = r['items'][0]['id']
            self.city_title = r['items'][0]['title']
            return r['items'][0]['id']
        except (VkException, VkAuthError, VkAPIError, IndexError):
            return False

    def user_input_sex(self):
        write_msg(self.group_vk_api_session, self.message_user_id, 'Введите пол. 1 - женщина, 2 - мужчина, 0 - любой: ')

    def user_input_age_from(self):
        write_msg(self.group_vk_api_session, self.message_user_id, 'Введите нижнюю границу возраста: ')

    def user_input_age_to(self):
        write_msg(self.group_vk_api_session, self.message_user_id, 'Введите верхнюю границу возраста: ')

    def validate_user_message(self, message: str):
        if self.current_state == "user_input_profile_id":
            if not self.check_profile(message):
                write_msg(self.group_vk_api_session, self.message_user_id,
                          f'Профиль не доступен. Укажите верный id профиля: ')
            else:
                self.user_profile_id = message
                write_msg(self.group_vk_api_session, self.message_user_id, f'Пользователь найден, продолжаем...')
                self.current_state = "user_input_city"
                self.user_input_city()
        elif self.current_state == "user_input_city":
            if not self.get_vk_city_id_by_name(message):
                write_msg(self.group_vk_api_session, self.message_user_id,
                          f'Город не найден. Укажите верный город еще раз: ')
            else:
                write_msg(self.group_vk_api_session, self.message_user_id, f'Город найден - {self.city_title}')
                self.current_state = "user_input_sex"
                self.user_input_sex()

        elif self.current_state == "user_input_sex":
            try:
                sex_id = int(message)
            except Exception:
                write_msg(self.group_vk_api_session, self.message_user_id,
                          f'Повторите ввод пола. 1 - женщина, 2 - мужчина, 0 - любой: ')
            if sex_id not in range(0, 3):
                write_msg(self.group_vk_api_session, self.message_user_id,
                          f'Повторите ввод пола. 1 - женщина, 2 - мужчина, 0 - любой: ')
            else:
                write_msg(self.group_vk_api_session, self.message_user_id, f'Хорошо.')
                self.sex = sex_id
                self.current_state = "user_input_age_from"
                self.user_input_age_from()
        elif self.current_state == "user_input_age_from":
            try:
                age_from = int(message)
                if age_from not in range(1, 101):
                    write_msg(self.group_vk_api_session, self.message_user_id,
                              f'Еще раз укажите нижнюю границу возраста: ')
                else:
                    self.age_from = age_from
                    self.current_state = "user_input_age_to"
                    self.user_input_age_to()
            except Exception:
                write_msg(self.group_vk_api_session, self.message_user_id,
                          f'Еще раз укажите нижнюю границу возраста: ')
        elif self.current_state == "user_input_age_to":
            try:
                age_to = int(message)
                if age_to not in range(1, 101):
                    write_msg(self.group_vk_api_session, self.message_user_id,
                              f'Еще раз укажите нижнюю границу возраста: ')
                else:
                    self.age_to = age_to
                    self.current_state = "search_people"
                    write_msg(self.group_vk_api_session, self.message_user_id,
                              f'Начал поиск, пожалуйста подождите ...')
            except Exception:
                write_msg(self.group_vk_api_session, self.message_user_id,
                          f'Еще раз укажите нижнюю границу возраста: ')
        else:
            pass

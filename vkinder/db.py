import sqlalchemy as sq
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DSN = 'sqlite:///vkinder.db'
engine = sq.create_engine(DSN)
Base = declarative_base()
Session = sessionmaker(bind=engine)


class Person(Base):
    __tablename__ = "person"

    vk_id = sq.Column(sq.Integer, primary_key=True)
    firstname = sq.Column(sq.String(length=50))
    lastname = sq.Column(sq.String(length=50))


def create_schema():
    Base.metadata.create_all(bind=engine)


def get_saved_people():
    create_schema()
    session = Session()
    res = session.query(Person.vk_id).all()
    session.close()
    return [i[0] for i in iter(res)]


def save_top_people(top_people: list):
    create_schema()
    session = Session()

    for item in top_people:
        query = Person(vk_id=item['id'], firstname=item["firstname"], lastname=item["lastname"])
        session.add(query)
    session.commit()
    session.close()

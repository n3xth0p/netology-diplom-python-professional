# token for vk.com application
APPLICATION_TOKEN = "8eb206252ee8445ba272e2b2537b8954ee66f1c42f0767743002f405911be7bb00f90386510a6f678fa57"

# token for vk.com group 
VK_GROUP_TOKEN = "2d1c636300dd7e1631217a691cac5434d124b936d3ea22d0e4ea2790db068601ea92c7047ae1932759f39"

# delay between requests for vk.com API
TIME_DELAY_REQUEST = 0.5

# delay in event loop
TIME_DELAY_EVENT_LOOP = 0.1

# version api
VK_API_VERSION = "5.126"

import mock
from vkinder.input_data import UserInput


class TestUserInput:

    def setup(self):
        print("setup test")
        self.user_input = UserInput()

    @mock.patch('vk.API', return_value=[
        {'first_name': 'Sergey', 'id': 141369, 'last_name': 'Zhukov', 'can_access_closed': True, 'is_closed': True}])
    def test_check_profile_true(self, API):
        result_check_profile = self.user_input.check_profile(141369)
        assert result_check_profile == True

    @mock.patch('vk.API',
                return_value=[{'first_name': 'DELETED', 'id': 123, 'last_name': '', 'deactivated': 'deleted'}])
    def test_check_profile_false(self, API):
        result_check_profile = self.user_input.check_profile(123)
        assert result_check_profile == False

    @mock.patch('vk.API', return_value={'count': 2,
                                        'items': [{'id': 177, 'title': 'Выборг', 'area': 'Выборгский район',
                                                   'region': 'Ленинградская область'},
                                                  {'id': 1046449, 'title': 'Торфопредприятие Выборгское',
                                                   'area': 'Выборгский район',
                                                   'region': 'Ленинградская область'}]})
    def test_get_vk_city_id_by_name_found(self, API):
        city_name = 'выборг'
        city_id = self.user_input.get_vk_city_id_by_name(city_name)
        assert city_id == 177
        assert self.user_input.city_id == 177
        assert self.user_input.city_title == 'Выборг'

    @mock.patch('vk.API', return_value={'count': 0, 'items': []})
    def test_get_vk_city_id_by_name_not_found(self, API):
        city_name = 'ываываыва'
        city_id = self.user_input.get_vk_city_id_by_name(city_name)
        assert city_id == False

    def teardown(self):
        print("end test")

import mock
from pprint import pprint
from vkinder.get_data import people_search, get_people_vk_search
from vkinder.input_data import UserInput

SEARCH_RESULT = {'count': 3, 'items': [{'can_access_closed': True,
                                        'first_name': 'Мирослава',
                                        'id': 123,
                                        'is_closed': False,
                                        'last_name': 'Николаева',
                                        'track_code': 'aaa'},

                                       {'can_access_closed': True,
                                        'first_name': 'Елена',
                                        'id': 222,
                                        'is_closed': False,
                                        'last_name': 'Иванова',
                                        'track_code': 'bbb'},
                                       {'can_access_closed': True,
                                        'first_name': 'Ольга',
                                        'id': 333,
                                        'is_closed': False,
                                        'last_name': 'Петрова',
                                        'track_code': 'ccc'}]}

OUTPUT_RESULT = [{'firstname': 'Мирослава',
                  'id': 123,
                  'lastname': 'Николаева',
                  'mutual': 0,
                  'photo': [],
                  'profile_url': 'https://vk.com/id123'},
                 {'firstname': 'Елена',
                  'id': 222,
                  'lastname': 'Иванова',
                  'mutual': 0,
                  'photo': [],
                  'profile_url': 'https://vk.com/id222'},
                 {'firstname': 'Ольга',
                  'id': 333,
                  'lastname': 'Петрова',
                  'mutual': 0,
                  'photo': [],
                  'profile_url': 'https://vk.com/id333'}]


class TestGetData:
    def setup(self):
        self.data = UserInput('5.126')

    @mock.patch('vkinder.get_data.save_top_people', return_value=[])
    @mock.patch('vkinder.get_data.get_saved_people', return_value=[1, 2, 3])
    @mock.patch('vkinder.get_data.get_people_vk_search', return_value=SEARCH_RESULT)
    @mock.patch('vkinder.get_data.get_profile_photo', return_value=[])
    @mock.patch('vkinder.get_data.get_mutual_friends', return_value=0)
    def test_people_search(self, get_mutual_friends, get_profile_photo,
                           get_people_vk_search, get_saved_people, save_top_people):
        result = people_search(self.data)
        assert result == OUTPUT_RESULT

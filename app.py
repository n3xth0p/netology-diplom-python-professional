import time
import vk_api
from vkinder.input_data import UserInput
from vkinder.get_data import people_search
from vkinder.settings import VK_GROUP_TOKEN, TIME_DELAY_EVENT_LOOP
from vk_api.longpoll import VkLongPoll, VkEventType
from vkinder.chat import write_msg

if __name__ == "__main__":
    user_input = UserInput()
    user_input.group_vk_api_session = vk_api.VkApi(token=VK_GROUP_TOKEN)
    event_loop = VkLongPoll(user_input.group_vk_api_session)

    for event in event_loop.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.to_me:
                request = event.text
                user_input.message_user_id = event.user_id

                if user_input.current_state == 'initial':
                    if request.lower().replace("'", "") == "поиск":
                        user_input.current_state = "user_input_profile_id"
                        user_input.user_input_profile_id()
                    else:
                        write_msg(user_input.group_vk_api_session,
                                  user_input.message_user_id,
                                  f"Приветствую, я бот Vkinder. Для поиска пары "
                                  f"введите команду 'поиск' ваш vk id - {event.user_id}")

                else:
                    user_input.validate_user_message(request)

        elif user_input.current_state == "search_people":
            found_people = people_search(user_input)
            if len(found_people) == 0:
                write_msg(user_input.group_vk_api_session, user_input.message_user_id,
                          "К сожалению никто не найден. Попробуйте еще раз, введите команду - 'поиск'")
                user_input.current_state = 'initial'
            else:
                write_msg(user_input.group_vk_api_session, user_input.message_user_id,
                          "Найдены следующие люди")
                for person in found_people:
                    msg = f"{person['firstname']} {person['lastname']} {person['profile_url']}"
                    write_msg(user_input.group_vk_api_session, user_input.message_user_id, f"{msg}",
                              person['photo'])
                write_msg(user_input.group_vk_api_session, user_input.message_user_id,
                          "Чтобы попробовать еще раз, введите команду - 'поиск'")
                user_input.current_state = 'initial'
        time.sleep(TIME_DELAY_EVENT_LOOP)
